<?php 
if ( ! defined( 'ABSPATH' ) ) exit;

define('_TELEROM_', 0);

class TeleromObject{

	private $params 	= [];
	private $items 		= [];
	private $options 	= [];

	// $username, $password, $currencyId, $orderFieldsXml, $discount, $tax, $noOfPayments
	public function __construct($username, $password, $currencyId, 
		$items, $discount = 0, $tax = 0, $noOfPayments = 0){

		$this->params = array(
			"username"			=> $username,
			"password"			=> $password,
			"clientIp"			=> $_SERVER['REMOTE_ADDR'],
			"discount"			=> $discount,
			"tax"				=> $tax,
			// "currencyId"        => $currencyId,
			// "noOfPayments"      => $noOfPayments,
			// "chargeType"        => "FormDefault",
			"orderFieldsXml"    => ''
			);

		$this->items = $items;
	}

	public function getForm(){
		if(count($this->params) == 0)
			die('Havn\'t parameters');

		$env = $this->InsertOrder($this->params);

		$opts = array( 'http' => array( 'method' => "POST",
										'header' => "Content-Type: text/xml;charset=utf-8\r\nSOAPAction: \"http://www.easyform.co.il/InsertOrder\"\r\nContent-length: ".strlen($env)."\r\n", 
										'content' => $env )
				);
		$context = stream_context_create($opts);
		$handle = fopen("https://redirect.telepay.co.il/WS2/Orders.asmx", "r", false, $context);
		$orderId = fgetss($handle);

		return $orderId;
	}

	private function InsertOrder($params){
		$envelope = new DOMDocument('1.0', 'utf-8');

		$root = $envelope->createElement('soap12:Envelope');
		$root->setAttributeNS('http://www.w3.org/2000/xmlns/' ,'xmlns:xsi', 'http://www.w3.org/2001/XMLSchema-instance');
		$root->setAttributeNS('http://www.w3.org/2000/xmlns/' ,'xmlns:xsd', 'http://www.w3.org/2001/XMLSchema');
		$root->setAttributeNS('http://www.w3.org/2000/xmlns/' ,'xmlns:soap12', 'http://www.w3.org/2003/05/soap-envelope');
		
		$body = $envelope->createElement("soap12:Body");
		$content = $envelope->createElement('InsertOrder');
		$content->setAttributeNS('http://www.w3.org/2000/xmlns/' ,'xmlns', 'http://www.easyform.co.il/');
		
		foreach($params as $key=>$val){
			if($key == 'orderFieldsXml'){
				// $orderFieldsXml = $envelope->createElement('orderFieldsXml');
				$fields 		= $envelope->createElement('fields');

				foreach ($this->items['fields'] as $item) {
					$field = $envelope->createElement("field");
					$field->appendChild(new DOMElement('fieldName',$item['post_excerpt']));
					$field->appendChild(new DOMElement('fieldValue',$item['post_name']));
					$field->appendChild(new DOMElement('fieldPrice',$item['price']));
					$field->appendChild(new DOMElement('fieldQuantity',$item['quantity']));
					$field->appendChild(new DOMElement('fieldCatNo',''));
					$field->appendChild(new DOMElement('fieldCategory',''));

					$fields->appendChild($field);
				}

				// $orderFieldsXml->appendChild($fields);
				$content->appendChild(new DOMElement('orderFieldsXml', $envelope->saveXML($fields)));
				continue;
			}
			$content->appendChild(new DOMElement($key,$val));
		}
		
		$body->appendChild($content);
		$root->appendChild($body);
		$envelope->appendChild($root);
		
		//echo "<pre>";
		$xml = $envelope->saveXML();
		// file_put_contents(dirname(__FILE__)."/handlelog.log",  $xml);

		return $xml;
	}
}


 ?>