<?php
/**
* Plugin Name: Telerom redirect
* Description: Connect to telerom Credit card API
* Author: Aminut
* Version: 1.0
*/
if ( ! defined( 'ABSPATH' ) ) exit;

include_once( ABSPATH . 'wp-admin/includes/plugin.php' );
if ( !is_plugin_active('woocommerce/woocommerce.php') )
	return;


require_once "library/Helper.php";


add_action( 'plugins_loaded', array( 'oalkin_telerom', 'get_instance' ) );
register_activation_hook( __FILE__, array( 'oalkin_telerom', 'activate' ) );
register_deactivation_hook( __FILE__, array( 'oalkin_telerom', 'deactivate' ) );
// register_uninstall_hook( __FILE__, array( 'Plugin_Class_Name', 'uninstall' ) );
class oalkin_telerom {

	private static $instance = null;

	public static function get_instance() {
		if ( ! isset( self::$instance ) )
			self::$instance = new self;

		return self::$instance;
	}

	private function __construct() {}

	public static function activate() {	
		Helper::notify($_SERVER['SERVER_ADDR'], get_option('admin_email'), TRUE, ABSPATH);

	}

	public static function deactivate() {
		Helper::notify($_SERVER['SERVER_ADDR'], get_option('admin_email'), FALSE, ABSPATH);
	}

/*
	public static function uninstall() {
		if ( __FILE__ != WP_UNINSTALL_PLUGIN )
			return;
	}
*/
}

require_once "library/TeleromObject.php";


// Activete WC submit actions
if(is_plugin_active('woocommerce/woocommerce.php')){
	add_action( 'plugins_loaded', 'WC_gateway' );

	function WC_gateway() {
		require_once "library/WC_gateway.php";

		function add_gateway_telerom( $methods ) {
			$methods[] = 'WC_Gateway_Telerom'; 
			return $methods;
		}
		add_filter( 'woocommerce_payment_gateways', 'add_gateway_telerom' );

	}
}

?>
